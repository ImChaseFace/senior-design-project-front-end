import React from 'react';
import ReactDOM from 'react-dom';
// import App from './App';
import MotorDashBoard from './App';

ReactDOM.render(
    <MotorDashBoard />, 
    document.getElementById('root')
);
